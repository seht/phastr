<?php

namespace app\controllers;

class MasterController extends \phastr\init\mvc\Controller
{

	public function init()
	{
		$this->loader()->load('phastr/modules/Session');
		$this->loader()->load('phastr/modules/Validation');
	
		if ($this->route->params(0) == 'ajax') {
			if (!$this->validation->validate('request')) {
				$this->request->status(400);
				return;
			}
			return;
		}
		
		$this->session->render();

		$this->loader()->load('app/modules/Config');
		$this->loader()->load('app/modules/Access');
		$this->loader($this->view)->load('phastr/modules/Assets');

		$this->view->uri = $this->route->uri();
		$this->view->lang = $this->session->client('lang');
		$this->view->app_title = 'Phastr (demo)';
		
		$this->view->title = '';
		$this->view->body = '';
	}

	public function render()
	{
		if (($status = $this->request->status()) >= 400) {
			return;
		}
		if ($this->route->params(0) == 'ajax') {
			$object = $this->route->params(1);
			$this->view->response = $this->{$object}->request($this->request);
			$this->view->layout('app/views/layouts/request/json');
			return;
		}
		
		$this->view->assets->meta(['charset' => 'utf-8']);
		$this->view->assets->meta(['name' => 'author', 'content' => 'seht.xyz']);
		$this->view->assets->meta(['name' => 'viewport', 'content' => 'width=device-width,initial-scale=1.0']);
		$this->view->assets->style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css', 'external');
		$this->view->assets->style('assets/css/style.css', 'file', ['media' => 'all']);
		$this->view->assets->script('//code.jquery.com/jquery-1.11.3.min.js', 'external');
		$this->view->assets->script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js', 'external');
		
		$this->loader()->load('app/navs/TopNav')->get(['class' => 'nav navbar-nav']);
		$this->loader()->load('app/navs/UserNav')->get(['class' => ['nav', 'navbar-nav', 'pull-right']]);
		
		$this->topnav->render($this->view);
		$this->usernav->render($this->view);

		$this->view->assets->script('$.ajaxSetup({headers: {\'' . $this->session->token() . '\': \'' . $this->session->get('_request') . '\'}});', 'inline');
		$this->view->page = $this->view->render('app/views/pages/' . (strtolower($this->route->resource())) . '/' . $this->route->action());
		$this->view->layout('app/views/layouts/default');
	}
	
}
