<?php

namespace app\controllers;

class CliController extends \phastr\init\mvc\Controller
{

	protected $output = [];

	public function init()
	{
		// Testing a cli controller.
		$this->loader()->load('app/modules/Access');
		if (!$this->access->resolve('cli')) {
			$this->request->status(403);
			return;
		}
		$this->output[] = "This is an example command line controller application.";
	}

	public function index()
	{
	}

	public function testCommand($params = [])
	{
		// Prints the command "arguments".
		// Example usage: $ php index.php /cli/testcommand/arg1/arg2/arg3=example/or:such
		$this->output[] = print_r($params, true);
	}

	public function render()
	{
		if ($this->request->status() == 403) {
			$this->request->status(403);
			return;
		}
		print(implode(PHP_EOL, $this->output));
		print(PHP_EOL);
	}
	
}
