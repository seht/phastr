<?php

namespace app\controllers;

class Index extends MasterController
{

	public function index()
	{
	}

	public function privatePage()
	{
		if (!$this->access->permission('private')) {
			$this->request->status(403);
		}
	}

	public function exampleForms()
	{
		$this->loader()->load('app/forms/index/TestForm');
		$this->loader()->load('app/forms/index/SimpleForm');
		$this->loader()->load('app/forms/index/ServiceForm');
		
		$this->testform->imported_data = ['example' => ['data']];
		$this->testform->get($this->request, 'ajax/testform');
		$this->simpleform->get($this->request, 'ajax/simpleform');
		$this->serviceform->get($this->request, 'ajax/serviceform');

		$this->testform->render($this->view, ['title' => 'Example form', 'attr' => ['class' => 'form form-horizontal']]);
		$this->simpleform->render($this->view, ['title' => 'Simple form', 'attr' => ['class' => 'form form-horizontal']]);
		$this->serviceform->render($this->view, ['title' => 'OAuth Consumer-Provider-AES-encryption form', 'attr' => ['class' => ['form', 'form-horizontal']]]);
	}
	
}
