<?php

namespace app\controllers;

class Consumer extends \phastr\init\mvc\Controller
{

	public function request($params = [])
	{
		$url = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/provider/exampleservice/post';
		$oauth = [
			'consumer_key' => 'consumerx',
			'consumer_secret' => 'consumerx-secret',
			'token' => 'consumerx-token',
			'token_secret' => 'consumerx-token-secret'];
		
		$this->loader()->load('phastr/modules/Encryption');
		
		$data = ['foo' => 'bar'];
		
		$params['data'] = $this->encryption->encrypt($data, 'consumerx-token-secret');
		
		$this->loader()->load('app/modules/OAuthClient');
		$this->view->response = $this->oauthclient->request($url, $params, $oauth, 'post');
	}

	public function render()
	{
		print print_r($this->view->response, true);
	}
	
}
