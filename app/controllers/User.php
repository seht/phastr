<?php

namespace app\controllers;

class User extends MasterController
{

	public function init()
	{
		parent::init();
		
		$this->view->title = 'User';
	}

	public function index()
	{
		if (!$this->access->permission('private')) {
			$this->request->status(403);
		}
	}

	public function edit()
	{
		if (!$this->access->permission('private')) {
			$this->request->status(403);
		}
	}

	public function login()
	{
		$this->loader()->load('app/models/User');
		$this->loader()->load('app/forms/user/LoginForm');

		$this->loginform->user_model = $this->user;
		$this->loginform->get($this->request, 'ajax/loginform');
		$this->loginform->render($this->view, ['title' => 'Authentication form', 'attr' => ['class' => 'form form-horizontal']]);
	}

	public function register()
	{
		$this->loader()->load('app/models/User');
		$this->loader()->load('app/forms/user/RegisterForm');
		
		$this->view->title = 'New User Registration';
		
		$this->registerform->user_model = $this->user;
		$this->registerform->get($this->request, 'ajax/loginform');
		$this->view->body = $this->registerform->render($this->view, ['title' => 'Registration form', 'attr' => ['class' => 'form form-horizontal']]);
	}

	public function verify()
	{
		$this->loader()->load('app/models/User');
		
		if ($this->route->params(0) !== $this->session->key()) {
			$this->request->status(403);
			return;
		}
		if ($token = $this->route->params(1)) {
			if ($this->user->verify($token)) {
				$this->view->title = 'New User Verification';
				$this->view->body = $this->view->render('app/views/pages/register/verify');
				return;
			}
		}
		$this->request->status(403);
	}

	public function logout()
	{
		$this->session->drop('user');
	}
	
}
