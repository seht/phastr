<?php

namespace app\controllers;

class Provider extends \phastr\init\mvc\Controller
{

	protected $endpoint = '';

	public function init()
	{
		$this->endpoint = 'http://' . $_SERVER['SERVER_NAME'] . $this->route->utils::uri($this->route->route('path'));
	}

	public function exampleService()
	{
		$this->loader()->load('app/modules/OAuthServer');
		if ($request = $this->oauthserver->request($this->endpoint, $_GET, $_SERVER['REQUEST_METHOD'])) {
			$this->loader()->load('app/services/ExampleService');
			$response = $this->exampleservice->{$_SERVER['REQUEST_METHOD']}($request['request'], $request['consumer']);
			return $this->view->response = $response;
		}
		http_response_code(401);
	}

	public function render()
	{
		$this->view->layout('app/views/layouts/request/json');
	}
	
}
