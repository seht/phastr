<?php

namespace app\services;

class ExampleService
{
	
	use \phastr\Loader;

	public function get($request, $consumer)
	{
		$this->loader()->load('phastr/modules/Encryption');
		$data = $this->encryption->decrypt($request['data'], $consumer['token_secret']);
		
		return $this->response(200, ['decrypted' => $data]);
	}

	public function post($request, $consumer)
	{
		$this->loader()->load('phastr/modules/Encryption');
		$data = $this->encryption->decrypt($request['data'], $consumer['token_secret']);
		
		return $this->response(200, ['decrypted' => $data]);
	}

	protected function response($status = 200, $data = null)
	{
		http_response_code($status);
		return ['status' => $status, 'data' => $data];
	}
	
}
