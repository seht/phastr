<?php

namespace app\services;

class Media
{

	public function application($type)
	{
		switch ($type) {
			case 'json':
			case 'pdf':
			case 'zip':
			case 'rss+xml':
			case 'atom+xml':
			case 'soap+xml':
			case 'ogg':
				break;
			default:
		}
	}

	public function text($type)
	{
		switch ($type) {
			case 'xml':
			case 'plain':
			case 'javascript':
			case 'css':
				break;
			default:
		}
	}

	public function image($type)
	{
		switch ($type) {
			case 'jpeg':
			case 'gif':
			case 'png':
			case 'svg+xml':
				break;
			default:
		}
	}

	public function audio($type)
	{
		switch ($type) {
			case 'ogg':
			case 'mpeg':
			case 'mp4':
				break;
			default:
		}
	}

	public function video($type)
	{
		switch ($type) {
			case 'ogg':
			case 'mpeg':
			case 'mp4':
				break;
			default:
		}
	}
	
}
