<?php

namespace app\forms\user;

use phastr\modules\Form;

class LoginForm extends Form
{

	public function fields($request = null)
	{
		$this->input('login_email', 'Email', ['attr' => ['class' => 'form-control']]);
		
		$this->validate('login_email', 'email');
		$this->validate('login_email', ['maxlength' => 128]);
		
		$this->input('login_password', 'Password', ['type' => 'password', 'attr' => ['class' => 'form-control']]);
		
		$this->validate('login_password', ['maxlength' => 32], 'max');
		$this->validate('login_password', ['minlength' => 8], 'min');
		
		$this->button('login_submit', 'Sign in', ['attr' => ['class' => ['btn', 'btn-primary']]]);
	}

	public function submit($request = null, $values = null, $status = null)
	{
		$this->message('You have successfully signed in', 'success');
		$this->message('Invalid credentials', 'error');
		
		if (!$this->user_model->login($values['login_email'], $values['login_password'])) {
			$this->error();
		}
	}
	
}
