<?php

namespace app\forms\user;

use phastr\modules\Form;

class RegisterForm extends Form
{

	public function fields($request = null)
	{
		$this->loader()->load('app/modules/Vocab');
		
		$this->input('register_name', 'Name', ['attr' => ['class' => 'form-control']]);
		
		$this->validate('register_name', ['maxlength' => 64]);
		
		$this->input('register_email', 'Email', ['attr' => ['class' => 'form-control']]);
		
		$this->validate('register_email', 'email');
		$this->validate('register_email', ['maxlength' => 128]);
		
		$this->input('register_password', 'Password', ['type' => 'password', 'attr' => ['class' => 'form-control']]);
		
		$this->validate('register_password', ['maxlength' => 32]);
		$this->validate('register_password', ['minlength' => 8]);
		
		$this->button('register_submit', 'Submit', ['attr' => ['class' => ['btn', 'btn-primary']]]);
		
		$this->message($this->vocab->t('register_success', 'user'), 'success');
		$this->message($this->vocab->t('register_fail', 'user'), 'error');
	}

	public function submit($request = null, $values = null, $status = null)
	{
		if (!$this->user_model->register($values['register_name'], $values['register_email'], $values['register_password'])) {
			$this->error();
		}
	}
	
}
