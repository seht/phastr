<?php

namespace app\forms\index;

use phastr\modules\Form;

class ServiceForm extends Form
{

	public function fields($request = null)
	{
		$this->input('test_service_field', ' ', ['attr' => ['class' => 'form-control']]);
		
		$this->validate('test_service_field', 'require');
		
		$this->button('submit_button', 'Submit', ['attr' => ['class' => ['btn', 'btn-primary']]]);
	}

	public function submit($request = null, $values = null, $status = null)
	{
		$this->loader()->load('phastr/modules/Encryption');
		$this->loader()->load('app/modules/OAuthClient');
		
		$url = 'http://' . $_SERVER['SERVER_NAME'] . '/index.php/provider/exampleservice/post';
		$oauth = [
			'consumer_key' => 'consumerx',
			'consumer_secret' => 'consumerx-secret',
			'token' => 'consumerx-token',
			'token_secret' => 'consumerx-token-secret'];
		$params['data'] = $this->encryption->encrypt($values, 'consumerx-token-secret');
		$response = $this->oauthclient->request($url, $params, $oauth, 'post');
		
		$this->message('Went through!', 'success');
		
		$this->callback('form_callback', serialize($response));
	}
	
}
