<?php

namespace app\forms\index;

use phastr\modules\Form;

class SimpleForm extends Form
{

	public function fields()
	{
		$this->input('test_custom_validation_field', ' ', ['attr' => ['placeholder' => 'A number that starts with 123...', 'class' => 'form-control']]);
		
		$this->validate('test_custom_validation_field', 'require');
		$this->validate('test_custom_validation_field', 'int', 'That\'s not a number!');
		
		$this->input('test_number_field_optional', ' ',
			['type' => 'number', 'attr' => ['placeholder' => 'Optional number field...', 'class' => 'form-control']]);
		
		$this->button('submit_button', 'Submit', ['attr' => ['class' => ['btn', 'btn-primary'], 'data-loading-text' => 'Loading...']]);
		
		$this->expire(false);
		$this->message(
			'<strong>Good job...</strong><br>The token for this form will not "expire" during the current session (so it may be re-submitted successfully without refreshing the page).',
			'success');
	}

	public function submit($request = null, $values = null)
	{
		if (strpos($values['test_custom_validation_field'], '123') !== 0) {
			$this->error('test_custom_validation_field', 'That number doesn\'t start with "123".');
			$this->message('Wrong number!', 'error');
		} else {
			$this->success('test_custom_validation_field', 'Nice. ' . $values['test_custom_validation_field'] . ' starts with "123".');
		}
	}
	
}
