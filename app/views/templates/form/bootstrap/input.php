<?php if ($field['label']['value']) : ?>
<label <?= $this->utils::attrs($field['label']['attr']); ?>>
<?php endif; ?>
<?= $field['label']['value']; ?>
<?php if (isset($field['prepend'])) : ?>
    <?= $field['prepend']; ?>
<?php endif; ?>
    <input data-toggle="tooltip" data-placement="top"
	<?= $this->utils::attrs($field['attr']); ?>>
<?php if (isset($field['append'])) : ?>
    <?= $field['append']; ?>
<?php endif; ?>
<?php if ($field['label']['value']) : ?>
    </label>
<?php endif; ?>
