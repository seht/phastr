<!DOCTYPE html>
<html>

<head>
<meta name="robots" content="noindex, nofollow">
<title><?= $error->getCode(); ?></title>
</head>

<body>
	<div id="node">
		<h1 class="title"><?= $error->getCode(); ?></h1>

		<div id="body">
			<h3><?= $error->getMessage(); ?></h3>
		</div>
	</div>
</body>

</html>
