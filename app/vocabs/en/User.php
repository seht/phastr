<?php

namespace app\vocabs\en;

class User
{

	const REGISTER_SUCCESS = 'User created successfully. Please verify your email address before signing in.';

	const REGISTER_FAIL = 'Unable to create user.';

	const REGISTER_VERIFY_EMAIL_SUBJECT = 'Please verify your email address';

	const REGISTER_VERIFY_EMAIL_BODY = 'Before signing in, please verify your email address by following the link below (during your current browser session).';
	
}
