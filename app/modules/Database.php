<?php

namespace app\modules;

use app\configs\Database as DatabaseConfig;

class Database extends \phastr\modules\Database
{

	private static $url;
	
	public function __construct($url = DatabaseConfig::URL, $options = [])
	{
		$url = $this->getUrl($url);
		$dsn = "{$url['scheme']}:host={$url['host']};dbname={$url['path']}";

		parent::__construct($dsn, $url['user'], $url['pass'], $options);
	}
	
	public function getUrl($url = null)
	{
		if (isset(self::$url)) {
			return self::$url;
		}
		if ($env_url = getenv('DATABASE_URL')) {
			$url = $env_url;
		}
		$url = parse_url($url);
		if ($url['scheme'] == 'postgres') {
			$url['scheme'] = 'pgsql';
		}
		$url['path'] = trim($url['path'], '/');
		return self::$url = $url;
	}
	
}
