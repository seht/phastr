<?php

namespace app\modules;

class Access
{
	
	use \phastr\Loader;

	public function __construct()
	{
		$this->loader()->init('phastr/Route');
		$this->loader()->load('phastr/modules/Session');
		$this->loader()->load('app/models/User');
	}

	public function isAuth()
	{
		return ($this->session->keygen($this->session->key()) && $this->session->token() === $this->user->token($this->session->get(['user' => 'id'])));
	}

	public function permission($rule, $perm = null, $role = null)
	{
		return $this->resolve($rule, $perm, $role);
	}

	public function resolve($rule, $perm = null, $role = null)
	{
		switch ($rule) {
			case 'public':
				return ($this->isAuth() === false);
			case 'private':
				return ($this->isAuth() === true);
			case 'role':
				if (!$perm || !$role || $this->isAuth() === false) {
					return false;
				}
				$perm = array_intersect((array) $perm, (array) $role);
				if ($perm) {
					return $perm;
				}
				return false;
			case 'cli':
				return (!empty($this->route->argv()));
			default:
				return false;
		}
	}
	
}
