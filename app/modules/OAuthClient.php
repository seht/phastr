<?php

namespace app\modules;

use OAuth;
use OAuthException;

class OAuthClient
{

	const METHOD = OAUTH_HTTP_METHOD_GET;

	const SIG = OAUTH_SIG_METHOD_HMACSHA256;

	const TYPE = OAUTH_AUTH_TYPE_URI;

	public $oauth, $info, $response;

	public function request($url, $params, $oauth, $method = self::METHOD, $sig = self::SIG, $type = self::TYPE)
	{
		try {
			$this->oauth = new OAuth($oauth['consumer_key'], $oauth['consumer_secret'], $sig, $type);
			
			$this->oauth->setToken($oauth['token'], $oauth['token_secret']);
			$this->oauth->setNonce(hash('sha1', uniqid(mt_rand(), true)));
			$this->oauth->setTimestamp(microtime(true));
			
			$this->oauth->fetch($url, $params, strtoupper($method));
			
			$this->info = $this->oauth->getLastResponseInfo();
			$this->response = $this->oauth->getLastResponse();
		} catch (OAuthException $ex) {
			print $ex->lastResponse;
			return;
		}
		return $this->response;
	}
	
}
