<?php

namespace app\modules;

class Config
{

	public function k($const, $context)
	{
		return $this->constant($const, $context);
	}

	public function ini($path, $sections = true)
	{
		return parse_ini_file($path . '.ini', $sections);
	}

	protected function constant($const, $context)
	{
		return constant('\\app\\configs\\' . $context . '::' . $const);
	}
	
}
