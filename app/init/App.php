<?php

namespace app\init;

use phastr\Route;
use phastr\modules\Request;
use phastr\handlers\RequestException;
use app\init\View;

class App extends \phastr\init\Mvc
{
	
	public function __construct(Route $route, Request $request)
	{
		parent::__construct($route, $request, new View());
		
		if ($request->status() >= 400) {
			throw new RequestException($request, $this->view, $request->status());
		}
	}
	
}
