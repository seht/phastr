<?php

namespace app\init;

use phastr\utils\Html;

class View extends \phastr\modules\View
{

	public $utils;
	
	public function __construct()
	{
		$this->utils = new Html();
	}
	
	public static function error($path = null, $ext = '.php')
	{
		if (!isset($path)) {
			$path = 'app/views/layouts/error';
		}
		return $path . $ext;
	}
	
}
