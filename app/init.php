<?php
$routes = [
	'index' => 'app/controllers/Index',
	'user' => 'app/controllers/User',
	'example-blog' => 'app/controllers/ExampleBlog',
	'consumer' => 'app/controllers/Consumer',
	'provider' => 'app/controllers/Provider',
	'cli' => 'app/controllers/CliController'];

$route = new \phastr\Route($routes, 'index', 'index', (isset($argv)) ? $argv : null);
$request = new phastr\modules\Request();
$view = new \app\init\View();

new \app\init\App($route, $request, $view);
