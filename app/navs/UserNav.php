<?php

namespace app\navs;

class UserNav extends \phastr\modules\Nav
{

	public function items()
	{
		$this->loader()->load('app/modules/Access');
		
		if ($this->access->isAuth()) {
			$this->item('Sign out', 'user/logout');
		} else {
			$this->item('Sign in', 'user/login');
			$this->item('Sign up', 'user/register');
		}
	}
	
}
