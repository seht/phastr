<?php

namespace phastr;

use phastr\utils\Path;

class Route
{

	public $utils;

	protected $route;

	public function __construct(array $routes, string $def_resource, string $def_action, $argv = null)
	{
		if (empty($argv)) {
			$route['uri'] = (isset($_SERVER['PATH_INFO'])) ? $_SERVER['PATH_INFO'] : '/';
		} else {
			$route['uri'] = (isset($argv[1])) ? $argv[1] : '/';
			array_shift($argv);
		}
		$route['path'] = array_filter(explode('/', trim($route['uri'], '/ '))) + [$def_resource, $def_action];
		$route['resource'] = (isset($routes[$route['path'][0]])) ? $routes[$route['path'][0]] : null;
		$route['route'] = $route['path'];
		$route['params'] = array_splice($route['route'], 2);
		$route['path'] = implode('/', $route['path']);
		$route['file'] = $_SERVER['SCRIPT_NAME'];
		$route['base'] = dirname($route['file']);
		$route['argv'] = $argv;
		
		$this->route = $route;
		$this->utils = new Path($route);
	}

	public function uri(): string
	{
		return $this->route['uri'];
	}

	public function argv(): array
	{
		return (array) $this->route['argv'];
	}

	public function route(string $name = null)
	{
		if (!isset($name)) {
			return $this->route;
		}
		return (isset($this->route[$name])) ? $this->route[$name] : null;
	}

	public function resource(bool $class = false)
	{
		if (!isset($this->route['resource'])) {
			return null;
		}
		return (!$class) ? basename($this->route['resource']) : $this->utils::getClass($this->route['resource']);
	}

	public function action(): string
	{
		return $this->route['route'][1];
	}

	public function params(int $index = null)
	{
		if (!isset($index)) {
			return $this->route['params'];
		}
		return (isset($this->route['params'][$index])) ? $this->route['params'][$index] : null;
	}
	
}
