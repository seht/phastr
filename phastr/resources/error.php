<!DOCTYPE html>
<html>

<head>
<meta name="robots" content="noindex, nofollow">
<title><?= $message; ?></title>
</head>

<body>
	<section>
		<?php if ($code >= 400 && $code <= 500): ?>
		<h1><?= $message; ?></h1>
		<?php else: ?>
		<!-- 
		<?= print_r($debug, true); ?>
		-->
		<pre>
			<strong><?= $code ?></strong>&nbsp;<em><?= $message; ?></em>
			<strong><?= $file; ?>:<?= $line; ?></strong>
		</pre>
		<?php endif; ?>
	</section>
	<section>
		<ul>
			<li><a href="javascript:window.history.go(-1)">Back</a></li>
			<li><a href="/">Home</a></li>
		</ul>
	</section>
</body>

</html>


