<?php

namespace phastr\init;

use phastr\Route;
use phastr\modules\Request;
use phastr\modules\View;

class Mvc extends \phastr\Init
{

	public $request, $view, $controller;

	public function __construct(Route $route, Request $request, View $view)
	{
		parent::__construct($route);

		$this->request = $request;
		$this->view = $view;

		if (class_exists($dispatcher = $route->resource(true)) && method_exists($dispatcher, $action = $route->action())) {
			$this->controller = new $dispatcher($route, $request, $view);
			$this->controller->init($action, $params = $route->route('params'));
			if (!($status = $request->status()) || $status == 200) {
				$this->controller->{$action}($params);
				$this->controller->render($action, $params);
				return;
			}
		}

		$request->status(404);
		
	}
	
}
