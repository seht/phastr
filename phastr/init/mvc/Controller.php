<?php

namespace phastr\init\mvc;

use phastr\Route;
use phastr\modules\Request;
use phastr\modules\View;
use phastr\Loader;

abstract class Controller
{
	use Loader;

	public $route, $request, $view;

	public function __construct(Route $route, Request $request, View $view)
	{
		$this->route = $route;
		$this->request = $request;
		$this->view = $view;
		
		$this->request->status(200);
	}

	abstract public function init();

	abstract public function render();
	
}
