<?php

namespace phastr\handlers;

use Exception;

class Error
{

	public static function exception(object $error)
	{
		$layout = (isset($error->layout)) ? $error->layout : 'phastr/resources/error.php';
		self::output($error->getCode(), $error->getMessage(), $error->getFile(), $error->getLine(), $layout);
	}

	public static function error($errno, $errstr, $errfile, $errline)
	{
		print self::output($errno, $errstr, $errfile, $errline);
	}

	private static function output($code = null, $message = null, $file = null, $line = null, $layout = null)
	{
		if (!$message) {
			$message = $code;
		}
		$debug = print_r(array_reverse(debug_backtrace()), true);

		include($layout);
	}
	
}
