<?php

namespace phastr\handlers;

use Exception;
use phastr\modules\Request;
use phastr\modules\View;

class RequestException extends Exception
{
	public $view;
	public $request;
	public $layout;
	
	public function __construct(Request $request, View $view, int $code = 0, $message = null, $layout = null)
	{
		parent::__construct($message, $request->status($code));
		
		$this->request = $request;
		$this->view = $view;
		$this->layout = $layout;
	}

}