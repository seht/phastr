<?php

namespace phastr\utils;

class Path
{
	private static $route;
	
	public function __construct(array $route)
	{
		self::$route = $route;
	}

	public static function route(string $path = null, bool $rewrite = false): string
	{
		return self::uri(self::$route['path'] . ((isset($path)) ? '/' . $path : ''), $rewrite);
	}
	
	public static function getClass(string $path)
	{
		return '\\' . str_replace('/', '\\', $path);
	}

	public static function uri(string $path = null, bool $rewrite = false): string
	{
		return ((!$rewrite) ? self::$route['file'] : self::$route['base']) . ((isset($path)) ? '/' . trim($path, '/') : self::$route['uri']);
	}

	public static function base(string $path = null): string
	{
		return rtrim(self::$route['base'], '/') . '/' . $path;
	}

	public static function root(string $path = null): string
	{
		return (isset($path)) ? $_SERVER['DOCUMENT_ROOT'] . '/' . $path : $_SERVER['DOCUMENT_ROOT'];
	}

	public static function label($path = null)
	{
		return preg_replace('/[^a-z0-9_]/i', '_', $path);
	}

	public static function resolve(string $file): string
	{
		return stream_resolve_include_path($file);
	}

	public static function query($data = null): string
	{
		return urldecode(http_build_query($data));
	}
	
}
