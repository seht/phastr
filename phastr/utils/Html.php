<?php

namespace phastr\utils;

class Html
{

	public static function attrs($attr = [])
	{
		return ($attr = Helper::attrs($attr)) ? ' ' . implode(' ', Helper::iterJoin($attr, '="', '', '"')) : '';
	}
	
}
