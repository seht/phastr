<?php

namespace phastr\models;

class Form extends \phastr\Model
{

	public $name, $action, $method, $title;
	
	public $attrs = [], $fields = [], $hidden = [], $button = [], $weight = [], $values = [], $fieldset = [];
	
	public $validate = [], $status, $result, $message, $callback, $expire;
	
}