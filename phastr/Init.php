<?php

namespace phastr;

abstract class Init
{

	public static $init;

	public $route;

	public function __construct(Route $route)
	{
		self::$init = &$this;
		
		$this->route = $route;
	}
	
}
