<?php

namespace phastr;

use ReflectionClass;
use ReflectionException;
use phastr\utils\Path;

class InstanceLoader
{

	private $_instance;

	public function __construct(object $instance)
	{
		$this->_instance = &$instance;
	}

	public function load(string $path, $args = null, $prop = null)
	{
		if (!isset($prop)) {
			$prop = strtolower(basename($path));
		}
		return $this->_instance->{$prop} = $this->reflect(Path::getClass($path), $args);
	}

	public function init(string $path, $args = null, $prop = null)
	{
		if (!isset($prop)) {
			$prop = strtolower(basename($path));
		}
		if (isset(Init::$init->{$prop})) {
			return $this->_instance->{$prop} = Init::$init->{$prop};
		}
		return $this->_instance->{$prop} = Init::$init->{$prop} = $this->reflect(Path::getClass($path), $args);
	}

	protected function reflect($class, $args = null)
	{
		return (!isset($args)) ? new $class() : (new ReflectionClass($class))->newInstanceArgs((array) $args);
	}
	
}

trait Loader
{

	private $_loader;

	protected function loader(object $instance = null)
	{
		if (isset($instance)) {
			return (is_object($instance)) ? new InstanceLoader($instance) : null;
		}
		return (isset($this->_loader)) ? $this->_loader : $this->_loader = new InstanceLoader($this);
	}
	
}
