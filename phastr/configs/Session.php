<?php

namespace phastr\configs;

class Session
{

	const DATABASE = false;

	const LANG = 'en';

	const KEY = 'session%hashing';

	const ALGO = 'sha256';
	
}
