<?php

namespace phastr\configs;

class Hash
{

	const KEY = 'secret key';

	const ALGO = 'sha512';

	const CIPHER = '$2a$';

	const COST = 10;

	const SALT = 22;

	const CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	
}
