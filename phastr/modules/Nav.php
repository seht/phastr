<?php

namespace phastr\modules;

use phastr\Loader;
use phastr\utils\Helper;
use phastr\modules\View;

abstract class Nav
{
	
	use Loader;

	public $name, $nav = [], $items = [];

	abstract public function items();

	public function get($attrs = [])
	{
		$this->name = strtolower(Helper::className($this));
		
		$this->items();
		
		$attrs['id'] = $this->name;
		$this->nav['attr'] = Helper::attrs($attrs);
		$this->nav['items'] = $this->items;
		
		return $this->nav;
	}

	public function item($label = null, $path = null, $params = [])
	{
		$count = count($this->items);
		$index = ($count > 0) ? $count + 1 : 0;
		
		if ($params) {
			foreach ($params as $key => $val) {
				if (is_int($key)) {
					$this->items[$index]['label'] = $label;
					$this->items[$index]['path'] = $path;
					$this->items[$index]['item'][] = $val;
					continue;
				}
				$this->items[] = ['label' => $label, 'path' => $path, 'item' => $params];
				break;
			}
		} else {
			$this->items[] = ['label' => $label, 'path' => $path, 'item' => $params];
		}
	}
	
	public function render(View $view, $template = 'bootstrap')
	{
		return $view->{$this->name} = $view->render('app/views/templates/nav/' . $template, ['nav' => $this->nav]);
	}
	
}
