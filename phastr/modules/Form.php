<?php

namespace phastr\modules;

use phastr\Loader;
use phastr\modules\View;
use phastr\modules\Request;
use phastr\models\Status;
use phastr\models\Form as FormModel;
use phastr\utils\Helper;
use phastr\utils\Path;
use phastr\configs\Validation as ValidationConfig;

abstract class Form
{
	
	use Loader;

	protected $form, $request;

	abstract public function fields();

	abstract public function submit();
	
	public function __construct()
	{
		$this->loader()->load('phastr/modules/Session');
		$this->loader()->load('phastr/modules/Validation');
		
		$this->form = new FormModel();
		
		$this->form->name = strtolower(Helper::className($this));
		$this->form->method = 'post';
		
	}

	public function request(Request $request)
	{	
		$this->request = $request;
		
		foreach ($this->form->validate as $id => $rules) {
			foreach ($rules as $rule) {
				$this->validation->resolve($id, $rule['rule'], $this->request->{$this->form->method}($id), $rule['message']);
			}
		}
		if (!$this->result()) {
			return $this->form->status->code;
		}
		$this->submit($this->request, $this->form->values = $this->request->{$this->form->method}(), $this->form->status->status);
		if (!$this->result()) {
			return $this->form->status->code;
		}
		if ((isset($this->form->expire)) ? $this->form->expire : $this->expire()) {
			$this->session->drop([$this->form->name => 'token']);
		}
		return $this->form->status;
	}

	public function get(Request $request, $route = null, $action = null)
	{
		$this->request = $request;
		
		if (!isset($action)) {
			if (!isset($route)) {
				$action = Path::route();
			} elseif (!empty($route)) {
				$action = Path::route($route);
			}
		}

		$this->form->action = $action;
		
		$this->secure();
		$this->fields($request);

		return $this;
	}

	public function render(View $view = null, $attrs = null, $template = 'bootstrap')
	{
		if ($this->request->isAjax()) {
			return null;
		}
		if (!isset($attrs)) {
			$attrs = [];
		} elseif (is_string($attrs)) {
			$this->form->title = $attrs;
		} elseif (isset($attrs['title'])) {
			$this->form->title = $attrs['title'];
		}
		$attrs['title'] = $this->form->title;
		$attrs['id'] = $this->form->name;
		$attrs['fields'] = $this->form->fields;
		$attrs['hidden'] = $this->form->hidden;
		$attrs['button'] = $this->form->button;
		$attrs['fieldset'] = $this->form->fieldset;

		if (!isset($attrs['attr'])) {
			$attrs['attr'] = [];
		}
		$attrs['attr']['id'] = $this->form->name;
		$attrs['attr']['action'] = $this->form->action;
		$attrs['attr']['method'] = $this->form->method;
		
		if (!isset($view)) {
			return $attrs;
		}
		return $view->{$this->form->name} = $view->render('app/views/templates/form/' . $template . '/form', ['form' => $attrs]);
	}

	protected function secure()
	{	
		if (!$this->session->get([$this->form->name => 'token'])) {
			$this->session->set([$this->form->name => 'token'], $this->session->hash($this->form->name, 'sha256'));
		}
		$session_token = $this->session->token();
		$request_token = $this->session->get('_request');
		
		$this->hidden('_request', $request_token);
		$this->hidden('_token', $this->session->get([$this->form->name => 'token']));
		
		$this->validate('_request', ['headers' => [$session_token => $request_token]]);
		$this->validate('_token', ['session' => [$this->form->name => 'token']]);
	}

	public function result()
	{
		$status = new Status();
		$status->code = ($this->validation->getResult(ValidationConfig::ERROR)) ? ValidationConfig::ERROR : ValidationConfig::SUCCESS;
		$status->message = (isset($this->form->message[$status->code])) ? $this->form->message[$status->code] : null;
		$status->validation = $this->validation->getResult();
		$status->callback = $this->form->callback;
		$status->expire = $this->form->expire;
		$status->status = ($status->code != ValidationConfig::ERROR);

		$this->form->status = $status;
		
		return $this->form->status->code;
	}

	public function validate($id, $rule = null, $message = null)
	{
		
		return $this->form->validate[$id][] = ['rule' => $rule, 'message' => $message];
	}

	public function status($id = null, $status = ValidationConfig::ERROR)
	{
		return ($id) ? $this->validation->getStatus($id, $status) : $this->validation->getResult($status);
	}

	public function error($id = null, $message = null)
	{
		return $this->validation->setStatus(($id) ? $id  : ValidationConfig::ERROR, ValidationConfig::ERROR, $message);
	}

	public function success($id = null, $message = null)
	{
		return $this->validation->setStatus(($id) ? $id : ValidationConfig::SUCCESS, ValidationConfig::SUCCESS, $message);
	}

	public function message($message = null, $status = ValidationConfig::SUCCESS)
	{
		return $this->form->message[$status] = $message;
	}

	public function callback($name, $args = null)
	{
		return $this->form->callback = ['name' => $name, 'args' => (array) $args];
	}

	public function expire($expire = true)
	{
		return $this->form->expire = (bool) $expire;
	}

	public function label($label = null)
	{
		if (!is_array($label)) {
			$label = ['value' => $label];
		}
		$label['attr'] = (isset($label['attr'])) ? (array) $label['attr'] : [];
		
		return $label;
	}

	protected function field($id, $field, $label = null)
	{
		if (!isset($field['fieldset'])) {
			$field['fieldset'] = 'default';
		}
		if (!isset($this->form->weight['group'][$id])) {
			$this->fieldset($field['fieldset'], $this->form->title, $id);
		}
		$this->form->weight['field'][$field['id']][] = $id;
		$this->form->weight['group'][$id][] = $field['id'];
		
		if ($label) {
			$this->form->fields[$id]['label'] = $this->label($label);
		}
		$this->form->fields[$id]['field'][] = $field;
		
		return $this->form->fields[$id];
	}

	public function fieldset($id, $title = null, $field_id = null)
	{
		if (!isset($this->form->fieldset[$id])) {
			$this->form->fieldset[$id] = ['title' => $title, 'fields' => []];
		}
		if ($field_id) {
			if (array_search($field_id, $this->form->fieldset[$id]['fields']) === false) {
				$this->form->fieldset[$id]['fields'][] = $field_id;
			}
		}
		return $this->form->fieldset[$id];
	}

	public function input($id, $label = null, $input = null)
	{
		if (!is_array($input)) {
			$input = ['value' => $input];
		}
		if (!isset($input['value'])) {
			$input['value'] = '';
		}
		if (!isset($input['type'])) {
			$input['type'] = 'text';
		}
		if (!isset($input['label'])) {
			$input['label'] = '';
		}
		$input['id'] = $id;
		$input['control'] = 'input';
		$input['label'] = $this->label($input['label']);
		
		$input['attr'] = (isset($input['attr'])) ? (array) $input['attr'] : [];
		$input['attr']['value'] = $input['value'];
		$input['attr']['type'] = $input['type'];
		$input['attr']['name'] = $id;
		$input['attr']['id'] = $id;
		
		if (isset($input['group'])) {
			$id = $input['group'];
		}
		$weight = (isset($this->form->weight['field'][$input['id']])) ? count($this->form->weight['field'][$input['id']]) : 0;
		
		if ($weight > 0) {
			if ($weight < 2) {
				$occur = array_search($input['id'], $this->form->weight['group'][$id]);
				$this->form->fields[$id]['field'][$occur]['attr']['name'] .= '[]';
			}
			$input['attr']['name'] .= '[]';
			$input['attr']['id'] .= '-' . $weight;
		}
		return $this->field($id, $input, $label);
	}

	public function select($id, $label = null, $select = [])
	{
		if (!isset($select['options'])) {
			$select['options'] = $select;
		}
		if (!isset($select['label'])) {
			$select['label'] = '';
		}
		$select['id'] = $id;
		$select['control'] = 'select';
		$select['label'] = $this->label($select['label']);
		
		$select['attr'] = (isset($select['attr'])) ? (array) $select['attr'] : [];
		$select['attr']['name'] = $id;
		$select['attr']['id'] = $id;
		
		if (isset($select['multiple'])) {
			$select['attr']['name'] .= '[]';
			$select['attr']['multiple'] = '';
		}
		foreach ($select['options'] as &$option) {
			$option['control'] = 'option';
			$option['attr'] = (isset($option['attr'])) ? (array) $option['attr'] : [];
			if (!isset($option['label'])) {
				$option['label'] = '';
			}
			if (!isset($option['value'])) {
				$option['value'] = '';
			}
			$option['attr']['value'] = $option['value'];
		}
		unset($option);
		
		if (isset($select['group'])) {
			$id = $select['group'];
		}
		return $this->field($id, $select, $label);
	}

	public function markup($id, $label = null, $markup = null)
	{
		if (!is_array($markup)) {
			$markup = ['value' => $markup];
		}
		if (!isset($input['value'])) {
			$input['value'] = '';
		}
		$markup['id'] = $id;
		$markup['control'] = 'markup';
		
		return $this->field($id, $markup, $label);
	}

	public function hidden($id, $hidden = null)
	{
		if (!is_array($hidden)) {
			$hidden = ['value' => $hidden];
		}
		if (!isset($hidden['value'])) {
			$hidden['value'] = '';
		}
		$hidden['id'] = $id;
		$hidden['control'] = 'hidden';
		
		$hidden['attr'] = (isset($hidden['attr'])) ? (array) $hidden['attr'] : [];
		$hidden['attr']['value'] = $hidden['value'];
		$hidden['attr']['type'] = 'hidden';
		$hidden['attr']['name'] = $id;
		$hidden['attr']['id'] = $id;
		
		return $this->form->hidden[$id] = $hidden;
	}

	public function button($id, $label = null, $button = [])
	{
		if (!isset($button['type'])) {
			$button['type'] = 'submit';
		}
		$button['id'] = $id;
		$button['control'] = 'button';
		$button['label'] = $label;
		
		$button['attr'] = (isset($button['attr'])) ? (array) $button['attr'] : [];
		$button['attr']['type'] = $button['type'];
		$button['attr']['id'] = $id;
		
		return $this->form->button[$id] = $button;
	}
	
}
