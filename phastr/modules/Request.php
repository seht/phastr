<?php

namespace phastr\modules;

class Request
{
	
	public function __construct()
	{
		$this->headers = getallheaders();
	}
	
	public function headers(string $name = null)
	{
		if (!isset($name)) {
			return $this->headers;
		}
		return (isset($this->headers[$name])) ? $this->headers[$name] : null;
	}
	
	public function request(string $name = null)
	{
		if (!isset($name)) {
			return $_REQUEST;
		}
		return (isset($_REQUEST[$name])) ? $_REQUEST[$name] : null;
	}

	public function server(string $name = null)
	{
		if (!isset($name)) {
			return $_SERVER;
		}
		return (isset($_SERVER[$name])) ? $_SERVER[$name] : null;
	}

	public function post(string $name = null)
	{
		if (!isset($name)) {
			return $_POST;
		}
		return (isset($_POST[$name])) ? $_POST[$name] : null;
	}

	public function get(string $name = null)
	{
		if (!isset($name)) {
			return $_GET;
		}
		return (isset($_GET[$name])) ? $_GET[$name] : null;
	}
	
	public function status(int $code = null): int
	{
		if (!isset($code)) {
			return http_response_code();
		}
		http_response_code($code);
		return http_response_code();
	}
	
	public function isAjax(): bool
	{
		return ($this->headers('X-Requested-With') == 'XMLHttpRequest');
	}
	
}
