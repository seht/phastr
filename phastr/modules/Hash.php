<?php

namespace phastr\modules;

use phastr\configs\Hash as HashConfig;

class Hash
{

	public function gen($data = null, $algo = HashConfig::ALGO, $key = HashConfig::KEY)
	{
		return ($key) ? hash_hmac($algo, $data, $key) : hash($algo, $data);
	}

	public function cipher($data = null)
	{
		return crypt($data, HashConfig::CIPHER . HashConfig::COST . '$' . $this->rand(HashConfig::SALT));
	}

	public function resolve($hash, $data = null, $algo = HashConfig::ALGO, $key = HashConfig::KEY)
	{
		if ($algo) {
			$subj = $this->gen($data, $algo, $key);
		} else {
			$subj = crypt($data, substr($hash, 0, strlen(HashConfig::CIPHER . HashConfig::COST) + 1 + HashConfig::SALT));
		}
		return ($hash === $subj);
	}

	public function rand($length = 16, $algo = null, $chars = HashConfig::CHARS)
	{
		$limit = strlen($chars) - 1;
		$rand = '';
		
		for ($i = 0; $i < (int) $length; $i++) {
			$index = mt_rand(0, $limit);
			$rand .= $chars[$index];
		}
		return (!$algo) ? $rand : hash($algo, uniqid($rand, true));
	}
	
}
