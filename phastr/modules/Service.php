<?php

namespace phastr\modules;

use phastr\utils\Helper;
use phastr\utils\Path;

class Service
{

	public $handle, $headers, $response, $info, $header, $result;

	public function __construct()
	{
		$this->handle = curl_init();
		$this->headers = null;
		$this->response = null;
		$this->info = null;
		$this->header = null;
		$this->result = null;
	}

	public function request($url, $data = null, $method = 'post')
	{
		self::__construct();
		
		curl_setopt($this->handle, CURLOPT_HEADER, true);
		curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);
		
		switch ($method) {
			case 'post':
				curl_setopt($this->handle, CURLOPT_POST, true);
				if (!empty($data)) {
					curl_setopt($this->handle, CURLOPT_POSTFIELDS, Path::query($data));
				}
				break;
			case 'get':
				curl_setopt($this->handle, CURLOPT_HTTPGET, true);
				if (!empty($data)) {
					$url .= '?' . Path::query($data);
				}
				break;
			case 'put':
				curl_setopt($this->handle, CURLOPT_PUT, true);
				curl_setopt($this->handle, CURLOPT_BINARYTRANSFER, true);
				break;
			default:
				return false;
		}
		
		curl_setopt($this->handle, CURLOPT_URL, $url);
		
		return true;
	}

	public function execute()
	{
		$this->response = curl_exec($this->handle);
		$this->info = curl_getinfo($this->handle);
		$this->header = trim(substr($this->response, 0, $header_size = (int) $this->info['header_size']));
		$this->header = Helper::args(Helper::filterSplit(PHP_EOL, $this->header));
		$this->result = trim(substr($this->response, $header_size));
		
		curl_close($this->handle);
		
		return $this->result;
	}

	public function response()
	{
		if (!isset($this->result)) {
			return $this->execute();
		}
		return $this->result;
	}

	public function setHeader($headers = [])
	{
		$headers = Helper::iterJoin($headers, ': ');
		$this->headers = array_merge((array) $this->headers, $headers);
		
		curl_setopt($this->handle, CURLOPT_HTTPHEADER, $this->headers);
	}

	public function setOpt($params = [])
	{
		foreach ($params as $option => $value) {
			curl_setopt($this->handle, $option, $value);
		}
	}
	
}
