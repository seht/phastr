<?php

namespace phastr\modules;

use phastr\Loader;
use phastr\configs\Session as SessionConfig;

class Session extends \phastr\components\Session
{
	
	use Loader;

	public function __construct()
	{
		$this->start((SessionConfig::DATABASE) ? new \phastr\handlers\Session() : null);
	}

	public function generate()
	{
		$this->set(['_timestamp' => 0], $this->timestamp(true));
		$this->set('_token', $this->token(true));
		$this->set('_key', $this->keygen());
		$this->set(['_client' => 'lang'], SessionConfig::LANG);
	}

	public function register()
	{
		$this->set(['_client' => 'agent'], (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : null);
	}

	public function render()
	{
		$this->set('_request', $this->hash($this->set(['_timestamp' => 1], $this->timestamp(true))));
	}

	public function timestamp($gen = false)
	{
		if (!$gen) {
			return $this->get('_timestamp');
		}
		return uniqid(microtime(true) . '.', true);
	}

	public function token($gen = false, $algo = 'sha1')
	{
		if (!$gen) {
			return $this->get('_token');
		}
		return $this->loader()->load('phastr/modules/Hash')->gen($this->session_id, $algo, $this->timestamp()[0]);
	}

	public function key()
	{
		return $this->get('_key');
	}

	public function keygen($hash = null, $algo = SessionConfig::ALGO)
	{
		$key = $this->loader()->load('phastr/modules/Hash')->gen($this->get('_token'), $algo, SessionConfig::KEY);
		return (!isset($hash)) ? $key : ($hash === $key);
	}

	public function client($key = 'agent')
	{
		return $this->get(['_client' => $key]);
	}

	public function hash($data = null, $algo = SessionConfig::ALGO)
	{
		return $this->loader()->load('phastr/modules/Hash')->gen($data, $algo, $this->key());
	}
	
}
