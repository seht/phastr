<?php

namespace phastr\modules;

abstract class View
{

	public function layout($path, $data = null, $ext = '.php')
	{
		if (isset($data)) {
			extract($data);
		}
		include($path . $ext);
	}

	public function render($path, $data = null, $ext = '.php')
	{
		ob_start();
		
		if (isset($data)) {
			extract($data);
		}
		include($path . $ext);
		
		return ob_get_clean();
	}
	
}
