<?php

namespace phastr\modules;

/**
 * Has AES needs certs, CA, gnupg...
 */
class Encryption
{

	const CIPHER = 'AES-256-CBC';

	protected function iv(string $cipher = self::CIPHER): string
	{
		return openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher));
	}

	public function encrypt($data, string $key, string $cipher = self::CIPHER, string $iv = ''): string
	{
		$encrypted = '';
		if (!$iv) {
			$iv = $encrypted = $this->iv();
		}
		return $encrypted .= openssl_encrypt(serialize($data), $cipher, $key, 0, $iv);
	}

	public function decrypt($data, string $key, string $cipher = self::CIPHER, string $iv = '')
	{
		if (!$iv) {
			$iv = substr($data, 0, openssl_cipher_iv_length($cipher));
			$data = substr($data, strlen($iv));
		}
		return unserialize(openssl_decrypt($data, $cipher, $key, 0, $iv));
	}
	
}

